var appName = 'Ecotrust',
	appVersion = '0.0.1',
	app = new Framework7( {
	    modalTitle: appName,
	    material: true,
		animatePages: true
	} ),
	$$ = Dom7,
	mainView = app.addView( '.view-main', {
	    domCache: true,
		dynamicNavbar: true
	} ),
	setupStatusInterval = null,
	downloadPagination = {
		page: 1,
		per_page: 15
	},
	db = window.openDatabase( 'com.ecotrust', appVersion, appName, 1000000 ),
	currentUser = {
		id: null,
		remote_id: null,
		name: {
			first: null,
			last: null,
			user: null
		},
		districts: null
	},
	apiBase = 'http://ggrids.com/ecotrust/api/',
	district = null,
	subCounty = null,
	group = null;

$$( '.app-name' ).text( appName );

document.addEventListener( 'deviceready', function() {
	$$( document ).on( 'change', '#belong_to_village', function (e) {
			if ( $$( this ).prop( 'checked' ) == true )
				$$( 'li#hide_show_village' ).removeClass( 'hide' );
			else
				$$( 'li#hide_show_village' ).addClass( 'hide' );
			
	});
	
	$$( document ).on( 'change', '#income_increase', function (e) {
			if ( $$( this ).prop( 'checked' ) == true )
				$$( 'li#hide_show_amount' ).removeClass( 'hide' );
			else
				$$( 'li#hide_show_amount' ).addClass( 'hide' );
			
	});
	
	$$( document ).on( 'change', '#has_improved_stove', function (e) {
			if ( $$( this ).prop( 'checked' ) == true )
				$$( 'li#hide_show_stove' ).removeClass( 'hide' );
			else
				$$( 'li#hide_show_stove' ).addClass( 'hide' );
			
	});
	
	$$( document ).on( 'change', '#attend_workshop', function (e) {
			if ( $$( this ).prop( 'checked' ) == true )
				$$( 'li#hide_show_organiser, li#hide_show_venue, li#hide_show_about' ).removeClass( 'hide' );
			else
				$$( 'li#hide_show_organiser, li#hide_show_venue, li#hide_show_about' ).addClass( 'hide' );
			
	});
	
	$$( 'a#logout' ).on( 'click', function () {
		window.localStorage.removeItem( 'currentUser' );
		app.closePanel();
		openLoginScreen();
	} );

	if ( window.localStorage.getItem( 'databaseSetup' ) == null )
		createDatabaseTables();

	if ( window.localStorage.getItem( 'installationRegistered' ) == null )
		registerInstallation();

	else {
		if ( window.localStorage.getItem( 'setOneSetup' ) == null )
			downloadData( 'countries' );

		else {
			if ( window.localStorage.getItem( 'currentUser' ) != null ) {
				currentUser = JSON.parse( window.localStorage.getItem( 'currentUser' ) );

				if ( window.localStorage.getItem( 'technicianSetup' ) == null )
					setupTechnician();
			}

			else
				openLoginScreen();
		}
	}
}, false );

document.addEventListener( 'backbutton', function() {
	$$( '.back' ).click();
}, false);

function setSetupStatus( status ) {
	$$( '#setup-loader' ).css( {
		'margin-top' : parseInt( ( $$( window ).height() - 100 ) / 2 ) + 'px'
	} );

	removeSetupStatus();

	app.popup( '#setup-screen', false, true );

	$$( '#setup-status span' ).text( status );

	setupStatusInterval = setInterval( function() {
		var container = $$( '#setup-status em' ),
			length = container.text().length;

		if ( length == 0 )
			container.text( '.' );

		else if ( length == 1 )
			container.text( '..' );

		else if ( length == 2 )
			container.text( '...' )

		else
			container.text( '' );
	}, 500 );
}

function removeSetupStatus() {
	if ( !setupStatusInterval == null || typeof setupStatusInterval != 'undefined' )
		clearInterval( setupStatusInterval );
}

function registerInstallation() {
	setSetupStatus( 'Registering installation' );

	app.popup( '#setup-screen', false, true );

	$$.post(
		apiBase + 'inbound',
		{
			method: 'register_installation',
			version: appVersion,
			device_info: JSON.stringify( device )
		},
		function( response ) {
			var response = JSON.parse( response );

			setTimeout( function() {
				if ( response.result == 'success' ) {
					window.localStorage.setItem( 'token', response.data.token );
					window.localStorage.setItem( 'id', response.data.id );

					window.localStorage.setItem( 'installationRegistered', 1 );

					downloadData( 'countries' );
				}

				else {
					app.alert( 'Server not accessible. Try again later!' );

					setTimeout( function() {
						navigator.app.exit();
					}, 3000 );
				}
			}, 3000 );
		},
		function() {
			app.alert( 'Server not accessible. Try again later!' );

			setTimeout( function() {
				navigator.app.exit();
			}, 3000 );
		}
	);
}

function downloadData( entries ) {
	switch ( entries ) {
		case 'countries' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'countries' );

			setSetupStatus( 'Downloading countries' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_countries',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' )
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.countries.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.countries, function( index, country ) {
									tx.executeSql( 'INSERT INTO countries ( remote_id, code, name ) VALUES ( ?, ?, ? )', [ country.id, country.code, country.name ] );
								} );
							}, null );

						downloadPagination.page = response.countries < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'countries' );

						else
							downloadData( 'districts' );
					}

					else
						downloadData( 'countries' );
				},
				function() {
					download_data( 'countries' );
				}
			);

			break;

		case 'districts' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'districts' );

			setSetupStatus( 'Downloading districts' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_districts',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' )
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.districts.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.districts, function( index, district ) {
									tx.executeSql( 'INSERT INTO districts ( remote_id, country_id, name ) VALUES ( ?, ( SELECT id FROM countries WHERE remote_id = ? ), ? )', [ district.id, district.location.country, district.name ] );
								} );
							}, null );

						downloadPagination.page = response.districts < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'districts' );

						else
							downloadData( 'technicians' );
					}

					else
						downloadData( 'districts' );
				},
				function() {
					downloadData( 'districts' );
				}
			);

			break;

		case 'technicians' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'technicians' );

			setSetupStatus( 'Downloading technicians' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_technicians',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' )
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.technicians.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.technicians, function( index, technician ) {
									if ( technician.app_access.allowed ) {
										var districtIds;

										if ( technician.location.districts.length == 0 )
											districtIds = JSON.stringify( new Array() );

										else {
											tx.executeSql(
												'SELECT id FROM districts WHERE remote_id IN ( ' + technician.location.districts.join( ', ' ) + ' ) ',
												[],
												function( tx, result ) {
													var ids = new Array();

													for ( i = 0; i < result.rows.length; i++ )
														ids.push( result.rows.item( i ).id );

													districtIds = JSON.stringify( ids );
												},
												function( tx, error ) {
													districtIds = JSON.stringify( new Array() );
												}
											);
										}

										var delayInsertRecord = window.setInterval( function() {
											if ( districtIds != null ) {
												db.transaction( function( tx ) {
													tx.executeSql(
														'INSERT INTO technicians ( remote_id, first_name, last_name, user_name, district_ids, password ) VALUES ( ?, ?, ?, ?, ?, ? )',
														[ technician.id, technician.name.first, technician.name.last, technician.name.user, districtIds, technician.app_access.password ]
													);
												}, null );

												clearInterval( delayInsertRecord );
											}
										}, 50 );
									}
								} );
							}, null );

						downloadPagination.page = response.technicians < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'technicians' );

						else {
							window.localStorage.setItem( 'setOneSetup', 1 );

							openLoginScreen();

							setTimeout( function() {
								app.closeModal( '#setup-screen', true );
							}, 2000 );
						}
					}

					else
						downloadData( 'technicians' );
				},
				function() {
					downloadData( 'technicians' );
				}
			);

			break;

		case 'sub-counties' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'sub_counties' );

			setSetupStatus( 'Downloading sub-counties' );

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT remote_id FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
					var districts = new Array();

					for ( i = 0; i < result.rows.length; i++ )
						districts.push( result.rows.item( i ).remote_id );

					$$.post(
						apiBase + 'outbound',
						{
							method: 'get_sub_counties',
							page: downloadPagination.page,
							per_page: downloadPagination.per_page,
							token: window.localStorage.getItem( 'token' ),
							account: currentUser.remote_id,
							districts: districts
						},
						function( response ) {
							var response = JSON.parse( response );

							if ( response.result == 'success' ) {
								if ( response.sub_counties.length > 0 )
									db.transaction( function( tx ) {
										$$.each( response.sub_counties, function( index, sub_county ) {
											tx.executeSql( 'INSERT INTO sub_counties ( remote_id, district_id, name ) VALUES ( ?, ( SELECT id FROM districts WHERE remote_id = ? ), ? )', [ sub_county.id, sub_county.location.district, sub_county.name ] );
										} );
									}, null );

								downloadPagination.page = response.sub_counties < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

								if ( downloadPagination.page > 1 )
									downloadData( 'sub-counties' );

								else
									downloadData( 'villages' );
							}

							else
								downloadData( 'sub-counties' );
						},
						function() {
							downloadData( 'sub-counties' );
						}
					);
				}, function( tx, error ) {
					downloadData( 'sub-counties' );
				} );
			} );

			break;

		case 'villages' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'villages' );

			setSetupStatus( 'Downloading villages' );

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT remote_id FROM sub_counties', [], function( tx, result ) {
					var sub_counties = new Array();

					for ( i = 0; i < result.rows.length; i++ )
						sub_counties.push( result.rows.item( i ).remote_id );

						$$.post(
							apiBase + 'outbound',
							{
								method: 'get_villages',
								page: downloadPagination.page,
								per_page: downloadPagination.per_page,
								token: window.localStorage.getItem( 'token' ),
								account: currentUser.remote_id,
								sub_counties: sub_counties
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'success' ) {
									if ( response.villages.length > 0 )
										db.transaction( function( tx ) {
											$$.each( response.villages, function( index, village ) {
												tx.executeSql( 'INSERT INTO villages ( remote_id, sub_county_id, name ) VALUES ( ?, ( SELECT id FROM sub_counties WHERE remote_id = ? ), ? )', [ village.id, village.location.sub_county, village.name ] );
											} );
										}, null );

									downloadPagination.page = response.villages < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

									if ( downloadPagination.page > 1 )
										downloadData( 'villages' );

									else
										downloadData( 'banks' );
								}

								else
									downloadData( 'villages' );
							},
							function() {
								downloadData( 'villages' );
							}
						);
				}, function( tx, error ) {
					downloadData( 'villages' );
				} );
			} );

			break;

		case 'banks' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'banks' );

			setSetupStatus( 'Downloading banks' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_banks',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.banks.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.banks, function( index, bank ) {
									tx.executeSql( 'INSERT INTO banks ( remote_id, name ) VALUES ( ?, ? )', [ bank.id, bank.name ] );
								} );
							}, null );

						downloadPagination.page = response.banks < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'banks' );

						else
							downloadData( 'bank-branches' );
					}

					else
						downloadData( 'banks' );
				},
				function() {
					downloadData( 'banks' );
				}
			);

			break;

		case 'bank-branches' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'bank_branches' );

			setSetupStatus( 'Downloading bank branches' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_bank_branches',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.bank_branches.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.bank_branches, function( index, bank_branch ) {
									tx.executeSql( 'INSERT INTO bank_branches ( remote_id, bank_id, name ) VALUES ( ?, ( SELECT id FROM banks WHERE remote_id = ? ), ? )', [ bank_branch.id, bank_branch.bank, bank_branch.name ] );
								} );
							}, null );

						downloadPagination.page = response.bank_branches < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'bank-branches' );

						else
							downloadData( 'tech-specifications' );
					}

					else
						downloadData( 'bank-branches' );
				},
				function() {
					downloadData( 'bank-branches' );
				}
			);

			break;

		case 'tech-specifications' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'tech_specifications' );

			setSetupStatus( 'Downloading tech specifications' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_tech_specifications',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.tech_specifications.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.tech_specifications, function( index, tech_specification ) {
									tx.executeSql( 'INSERT INTO tech_specifications ( remote_id, name ) VALUES ( ?, ? )', [ tech_specification.id, tech_specification.name ] );
								} );
							}, null );

						downloadPagination.page = response.tech_specifications < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'tech-specifications' );

						else
							downloadData( 'tech-specification-descriptions' );
					}

					else
						downloadData( 'tech-specifications' );
				},
				function() {
					downloadData( 'tech-specifications' );
				}
			);

			break;

		case 'tech-specification-descriptions' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'tech_specification_descriptions' );

			setSetupStatus( 'Downloading tech specification descriptions' );

			$$.post(
				apiBase + 'outbound',
				{
					method: 'get_tech_specification_descriptions',
					page: downloadPagination.page,
					per_page: downloadPagination.per_page,
					token: window.localStorage.getItem( 'token' ),
					account: currentUser.remote_id
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'success' ) {
						if ( response.tech_specification_descriptions.length > 0 )
							db.transaction( function( tx ) {
								$$.each( response.tech_specification_descriptions, function( index, tech_specification_description ) {
									tx.executeSql( 'INSERT INTO tech_specification_descriptions ( remote_id, tech_specification_id, name ) VALUES ( ?, ( SELECT id FROM tech_specifications WHERE remote_id = ? ), ? )', [ tech_specification_description.id, tech_specification_description.tech_specification, tech_specification_description.name ] );
								} );
							}, null );

						downloadPagination.page = response.tech_specification_descriptions.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

						if ( downloadPagination.page > 1 )
							downloadData( 'tech-specification-descriptions' );

						else
							downloadData( 'producer-groups' );
					}

					else
						downloadData( 'tech-specification-descriptions' );
				},
				function() {
					downloadData( 'tech-specification-descriptions' );
				}
			);

			break;

		case 'producer-groups' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'producer_groups' );

			setSetupStatus( 'Downloading producer groups' );

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT remote_id FROM villages', [], function( tx, result ) {
					var villages = new Array();

					for ( i = 0; i < result.rows.length; i++ )
						villages.push( result.rows.item( i ).remote_id );

						$$.post(
							apiBase + 'outbound',
							{
								method: 'get_producer_groups',
								page: downloadPagination.page,
								per_page: downloadPagination.per_page,
								token: window.localStorage.getItem( 'token' ),
								account: currentUser.remote_id,
								villages: villages
							},
							function( response ) {
								var response = JSON.parse( response );

								if ( response.result == 'success' ) {
									if ( response.producer_groups.length > 0 )
										db.transaction( function( tx ) {
											$$.each( response.producer_groups, function( index, producer_group ) {
												tx.executeSql( 'INSERT INTO producer_groups ( remote_id, village_id, name ) VALUES ( ?, ( SELECT id FROM villages WHERE remote_id = ? ), ? )', [ producer_group.id, producer_group.location.village, producer_group.name ] );
											} );
										}, null );

									downloadPagination.page = response.producer_groups.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

									if ( downloadPagination.page > 1 )
										downloadData( 'producer-groups' );

									else
										downloadData( 'producers' );
								}

								else
									downloadData( 'producer-groups' );
							},
							function() {
								downloadData( 'producer-groups' );
							}
						);
				}, function( tx, error ) {
					downloadData( 'producer-groups' );
				} );
			} );

			break;

		case 'producers' :

			if ( downloadPagination.page == 1 )
				truncateTable( 'producers' );

			setSetupStatus( 'Downloading producers' );

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT remote_id FROM villages', [], function( tx, result ) {
					var villages = new Array();

					for ( i = 0; i < result.rows.length; i++ )
						villages.push( result.rows.item( i ).remote_id );

					$$.post(
						apiBase + 'outbound',
						{
							method: 'get_producers',
							page: downloadPagination.page,
							per_page: downloadPagination.per_page,
							token: window.localStorage.getItem( 'token' ),
							account: currentUser.remote_id,
							villages: villages
						},
						function( response ) {
							var response = JSON.parse( response );

							if ( response.result == 'success' ) {
								if ( response.producers.length > 0 )
									db.transaction( function( tx ) {
										$$.each( response.producers, function( index, producer ) {
											var groups_matched = false,
												banks_matched = false;
												bank_branches_matched = false;

											if ( producer.groups.length > 0 )
												$$.each( producer.groups, function( index, group ) {
													db.transaction( function( tx ) {
														tx.executeSql( 'SELECT id FROM producer_groups WHERE remote_id = ? LIMIT 1', [ group.group ], function( tx, result ) {
															if ( result.rows.length == 1 )
																producer.groups[ index ].group = result.rows.item( 0 ).id;

															else
																producer.groups.splice( group, 1 );

															if ( index == ( producer.groups.length - 1 ) )
																groups_matched = true;
														} );
													} );
												} );

											else
												groups_matched = true;

											if ( producer.banking.banks.length > 0 )
												$$.each( producer.banking.banks, function( index, bank ) {
													db.transaction( function( tx ) {
														tx.executeSql( 'SELECT id FROM banks WHERE remote_id = ? LIMIT 1', [ bank.bank ], function( tx, result ) {
															if ( result.rows.length == 1 )
																producer.banking.banks[ index ].bank = result.rows.item( 0 ).id;

															else
																producer.banking.banks.splice( bank, 1 );

															if ( index == ( producer.banking.banks.length - 1 ) )
																banks_matched = true;
														} );
													} );
												} );

											else
												banks_matched = true;

											if ( producer.banking.banks.length > 0 )
												$$.each( producer.banking.banks, function( index, bank ) {
													db.transaction( function( tx ) {
														tx.executeSql( 'SELECT id FROM bank_branches WHERE remote_id = ? LIMIT 1', [ bank.branch ], function( tx, result ) {
															if ( result.rows.length == 1 )
																producer.banking.banks[ index ].branch = result.rows.item( 0 ).id;

															else
																producer.banking.banks.splice( bank, 1 );

															if ( index == ( producer.banking.banks.length - 1 ) )
																bank_branches_matched = true;
														} );
													} );
												} );

											else
												bank_branches_matched = true;

											var  waitingInterval = setInterval( function() {
												if ( groups_matched && banks_matched && bank_branches_matched ) {
													db.transaction( function( tx ) {
														tx.executeSql(
															'INSERT INTO producers ( remote_id, village_id, code, first_name, other_names, gender, date_of_birth, phone_number, photo, producer_groups, banking_method, sacco_name, bank_details ) VALUES ( ?, ( SELECT id FROM villages WHERE remote_id = ? ), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )',
															[ producer.id, producer.location.village, producer.code, producer.name.first, producer.name.others, producer.gender, producer.dob, producer.phone, producer.photo, JSON.stringify( producer.groups ), producer.banking.method, producer.banking.sacco, JSON.stringify( producer.banking.banks ) ]
														);
													} );

													clearInterval( waitingInterval );
												}
											} );
										} );
									}, null );

								downloadPagination.page = response.producers.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

								if ( downloadPagination.page > 1 )
									downloadData( 'producers' );

								else {
									window.localStorage.setItem( 'technicianSetup', 1 );

									setTimeout( function() {
										app.closeModal( '#setup-screen', true );
									}, 2000 );
								}
							}

							else
								downloadData( 'producers' );
						},
						function() {
							downloadData( 'producers' );
						}
					);
				}, function( tx, error ) {
					downloadData( 'producer-groups' );
				} );
			} );

			break;
	}
}

function createDatabaseTables() {
	db.transaction( function( tx ) {
		var tables = [ 'settings', 'countries', 'districts', 'technicians', 'banks', 'bank_branches', 'tech_specifications', 'tech_specification_descriptions', 'producer_groups', 'producers' ];

		for ( index = 0; index < tables.length; index++ )
			tx.executeSql( 'DROP TABLE IF EXISTS ' + tables[ index ] );

		tx.executeSql( 'CREATE TABLE settings ( id INTEGER PRIMARY KEY AUTOINCREMENT, name CHAR(250), value TEXT )' );
		tx.executeSql( 'CREATE TABLE countries ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, code CHAR(3), name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE districts ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, country_id INTEGER, name CHAR(160) , created_at  CHAR(60) , updated_at CHAR(60))' );
		tx.executeSql( 'CREATE TABLE sub_counties ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, district_id INTEGER, name CHAR(160) , created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE villages ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, sub_county_id INTEGER, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE technicians ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, first_name CHAR(80), last_name CHAR(80), user_name CHAR(160), district_ids CHAR(15), password CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE banks ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE bank_branches ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, bank_id INTEGER, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE tech_specifications ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE tech_specification_descriptions ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, tech_specification_id INTEGER, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE producer_groups ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, village_id INTEGER, name CHAR(160) , created_at  CHAR(60) , updated_at CHAR(60))' );
		tx.executeSql( 'CREATE TABLE producers ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, village_id INTEGER, code CHAR(20), first_name CHAR(80), other_names CHAR(80), gender INTEGER, date_of_birth CHAR(10), phone_number CHAR(12), photo TEXT, producer_groups CHAR(160), banking_method INTEGER, sacco_name CHAR(160), bank_details TEXT , created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE tree_species ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name  CHAR(20), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE corrective_actions ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name  CHAR(20), created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE areas ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, producer_id  INTEGER, tgb_area_size TINYINT NOT NULL , gps CHAR(60) , land_Size TINYINT NOT NULL ,village_id INTEGER , created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE plantings ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, area_id  INTEGER, year TINYINT NOT NULL , tech_spec_version INTEGER , tech_spec_description INTEGER ,tree_species TEXT , created_at  CHAR(60) , updated_at CHAR(60) )' );
		tx.executeSql( 'CREATE TABLE monitorings ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, area_id  INTEGER, year TINYINT NOT NULL , tree_target INTEGER , correct_area INTEGER ,correct_tree_species INTEGER , notes TEXT , qualify INTEGER , tree_samples TEXT , tree_dying_cause CHAR(60) , created_at  CHAR(60) , updated_at CHAR(60) )' );
	}, null );
}

function getRecordCount( table ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT * FROM ' + table, [], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				console.log( result.rows.item( i ) );
		} );
	} );
}

function truncateTable( table ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'DELETE FROM ' + table );
	} );
}

function openLoginScreen() {
	$$( '#login-screen form' ).css( {
		'margin-top' : parseInt( ( $$( window ).height() - 250 ) / 2 ) + 'px'
	} );

	$$( '.login-screen-content form [name="username"], .login-screen-content form [name="password"]' ).val( '' );

	$$( '.login-screen-content form .list-button' ).off( 'click' ).on( 'click', function( event ) {
		event.preventDefault();

		var username = $$( '.login-screen-content form [name="username"]' ),
			password = $$( '.login-screen-content form [name="password"]' );

		if ( username.val().trim().length == 0 )
			username.focus();

		else if ( password.val().trim().length == 0 )
			password.focus();

		else {
			app.showPreloader();

			db.transaction( function( tx ) {
				tx.executeSql(
					'SELECT * FROM technicians WHERE user_name = ? LIMIT 1',
					[ username.val() ],
					function( tx, result ) {
						if ( result.rows.length > 0 ) {
							tx.executeSql(
								'SELECT * FROM technicians WHERE user_name = ? AND password = ? LIMIT 1',
								[ username.val(), password.val() ],
								function( tx, result ) {
									app.hidePreloader();

									if ( result.rows.length > 0 ) {
										var record = result.rows.item( 0 );

										currentUser.id = record.id;
										currentUser.remote_id = record.remote_id;
										currentUser.name.first = record.first_name;
										currentUser.name.last = record.last_name;
										currentUser.name.user = record.user_name;
										currentUser.districts = JSON.parse( record.district_ids );

										window.localStorage.setItem( 'currentUser', JSON.stringify( currentUser ) );

										if ( window.localStorage.getItem( 'technicianSetup' ) == null )
											setupTechnician();

										setTimeout( function() {
											app.closeModal( '#login-screen', true );
										}, 2000 );
									}

									else {
										app.hidePreloader();

										app.alert( 'Wrong password entered. Try again!' );
									}
								},
								null
							);
						}

						else {
							app.hidePreloader();

							app.alert( 'No technician found matching that username. Try again!' );
						}
					},
					null
				);
			} );
		}
	} );

	app.loginScreen( '#login-screen', true );
}

function setupTechnician() {
	if ( window.localStorage.getItem( 'installationMapped' ) == null ) {
		setSetupStatus( 'Mapping installation' );

		$$.post(
			apiBase + 'inbound',
			{
				method: 'map_installation',
				id: window.localStorage.getItem( 'id' ),
				token: window.localStorage.getItem( 'token' ),
				user: currentUser.remote_id
			},
			function( response ) {
				var response = JSON.parse( response );

				setTimeout( function() {
					if ( response.result == 'success' ) {
						window.localStorage.setItem( 'installationMapped', 1 );

						downloadData( 'sub-counties' );
					}

					else {
						app.alert( 'Server not accessible. Try again later!' );

						setTimeout( function() {
							navigator.app.exit();
						}, 3000 );
					}
				}, 3000 );
			},
			function() {
				app.alert( 'Server not accessible. Try again later!' );

				setTimeout( function() {
					navigator.app.exit();
				}, 3000 );
			}
		);
	}

	else
		downloadData( 'sub-counties' );
}

function loadDistricts() {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' ) ORDER BY name ASC', [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<li>' +
					'<a href="pages/sub-counties.html" data-district-id="' + record.id + '" class="item-link item-content">' +
						'<div class="item-inner">' +
							'<div class="item-title">' + record.name + '</div>' +
						'</div>' +
					'</a>' +
				'</li>';
			}

			$$( '.districts-list-block ul' ).html( html );

			$$( '.districts-list-block ul li a' ).on( 'click', function( event ) {
				district = $$( this ).attr( 'data-district-id' );
			} );
		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="districts"]', function( event ) {
	loadDistricts();
} );

function loadSubCounties() {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT name FROM districts WHERE id = ? LIMIT 1', [ district ], function( tx, result ) {
			if ( result.rows.length == 1 ) {
				$$( '.district-name' ).text( result.rows.item( 0 ).name );
			}
		} );

		tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ? ORDER BY name ASC', [ district ], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<li>' +
					'<a href="pages/villages.html" data-sub-county-id="' + record.id + '" class="item-link item-content">' +
						'<div class="item-inner">' +
							'<div class="item-title">' + record.name + '</div>' +
						'</div>' +
					'</a>' +
				'</li>';
			}

			$$( '.sub-counties-list-block ul' ).html( html );

			$$( '.sub-counties-list-block ul li a' ).on( 'click', function( event ) {
				subCounty = $$( this ).attr( 'data-sub-county-id' );
			} );
		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="sub-counties"]', function( event ) {
	loadSubCounties();

	$$( '.add-sub-county' ).off( 'click' ).on( 'click', function( event ) {
		app.prompt( 'Enter the sub-county name', 'Add sub-county', function ( name ) {
	        if ( name.trim().length > 0 ) {
				db.transaction( function( tx ) {
					tx.executeSql( 'SELECT id FROM sub_counties WHERE district_id = ? AND name = ?', [ district, name ], function( tx, result ) {
						if ( result.rows.length == 0 ) {
							tx.executeSql( 'INSERT INTO sub_counties ( district_id, name ) VALUES ( ?, ? )', [ district, name ], function( tx, result ) {
								loadSubCounties();
							} );
						}

						else
							app.alert( 'Sub-county already exists', 'Error' );
					} );
				} );
			}

			else {
				app.alert( 'Sub-county name provided not valid. Try again!', 'Error' );
			}
	    } );
	} );
} );

function loadVillages() {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT name FROM sub_counties WHERE id = ? LIMIT 1', [ subCounty ], function( tx, result ) {
			if ( result.rows.length == 1 ) {
				$$( '.sub-county-name' ).text( result.rows.item( 0 ).name );
			}
		} );

		tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ? ORDER BY name ASC', [ subCounty ], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<li>' +
					'<div class="item-content">' +
						'<div class="item-inner">' +
							'<div class="item-title">' + record.name + '</div>' +
						'</div>' +
					'</div>' +
				'</li>';
			}

			$$( '.villages-list-block ul' ).html( html );
		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="villages"]', function( event ) {
	loadVillages();

	$$( '.add-village' ).off( 'click' ).on( 'click', function( event ) {
		app.prompt( 'Enter the village name', 'Add village', function ( name ) {
	        if ( name.trim().length > 0 ) {
				db.transaction( function( tx ) {
					tx.executeSql( 'SELECT id FROM villages WHERE sub_county_id = ? AND name = ?', [ subCounty, name ], function( tx, result ) {
						if ( result.rows.length == 0 ) {
							tx.executeSql( 'INSERT INTO villages ( sub_county_id, name ) VALUES ( ?, ? )', [ subCounty, name ], function( tx, result ) {
								loadVillages();
							} );
						}

						else
							app.alert( 'Village already exists', 'Error' );
					} );
				} );
			}

			else {
				app.alert( 'Village name provided not valid. Try again!', 'Error' );
			}
	    } );
	} );
} );

function loadGroups() {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id, name FROM producer_groups ORDER BY name ASC', [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<li>' +
					'<a href="pages/group.html" data-group-id="' + record.id + '" class="item-link item-content">' +
						'<div class="item-inner">' +
							'<div class="item-title">' + record.name + '</div>' +
						'</div>' +
					'</a>' +
				'</li>';
			}

			$$( '.groups-list-block ul' ).html( html );

			$$( '.groups-list-block ul li a' ).on( 'click', function( event ) {
				group = $$( this ).attr( 'data-group-id' );
			} );
		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="groups"]', function( event ) {
	loadGroups();

	$$( '.close-add-group-screen' ).off( 'click' ).on( 'click', function( event ) {
		app.closeModal( '#add-group-screen', true );
	} );

	$$( '.add-group' ).off( 'click' ).on( 'click', function( event ) {
		app.popup( '#add-group-screen', false, true );
	} );
} );

$$( document ).on( 'page:init', '.page[data-page="add-group"]', function( event ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<option value="' + record.id + '">' +
					record.name +
				'</option>';
			}

			$$( '#group-district' ).html( html ).trigger( 'change' );
		} );
	} );

	$$( '#group-name' ).on( 'blur', function( event ) {
		if ( $$( this ).val().trim().length == 0 )
			$$( this ).focus();
	} ).focus();

	$$( '#group-district' ).off( 'change' ).on( 'change', function( event ) {
		var districtId = $$( this ).val();

		db.transaction( function( tx ) {
			tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ?', [ districtId ], function( tx, result ) {
				if ( result.rows.length > 0 ) {
					var html = '';

					for ( i = 0; i < result.rows.length; i++ ) {
						var record = result.rows.item( i );

						html += '<option value="' + record.id + '">' +
							record.name +
						'</option>';
					}

					$$( '#group-sub-county' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
				}
			} );
		} );
	} );

	$$( '#group-sub-county' ).off( 'change' ).on( 'change', function( event ) {
		var subCountyId = $$( this ).val();

		if ( subCountyId != '' ) {
			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ?', [ subCountyId ], function( tx, result ) {
					if ( result.rows.length > 0 ) {
						var html = '';

						for ( i = 0; i < result.rows.length; i++ ) {
							var record = result.rows.item( i );

							html += '<option value="' + record.id + '">' +
								record.name +
							'</option>';
						}

						$$( '#group-village' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
					}
				} );
			} );
		}
	} );

	$$( '#save-group' ).off( 'click' ).on( 'click', function( event ) {
		var name = $$( '#group-name' ),
			district = $$( '#group-district' ),
			subCounty = $$( '#group-sub-county' ),
			village = $$( '#group-village' );

		if ( name.val().trim().length == 0 )
			name.focus();

		else if ( village.val() == '' )
			village.focus();

		else {
			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT id FROM producer_groups WHERE village_id = ? AND name = ?', [ parseInt( village.val() ), name.val() ], function( tx, result ) {
					if ( result.rows.length == 0 ) {
						tx.executeSql( 'INSERT INTO producer_groups ( village_id, name ) VALUES ( ?, ? )', [ parseInt( village.val() ), name.val() ], function( tx, result ) {
							$$( '.back' ).click();

							loadGroups();
						} );
					}

					else {
						app.alert( 'Producer group already exists. Try again!', 'Error' );

						name.focus();
					}
				} );
			} );
		}
	} );
} );

function loadGroup() {
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT name FROM producer_groups WHERE id = ? LIMIT 1', [ group ], function( tx, result ) {
			if ( result.rows.length == 1 ) {
				$$( '.group-name' ).text( result.rows.item( 0 ).name );
			}
		} );

		tx.executeSql( "SELECT id, code, first_name, other_names, photo FROM producers WHERE producer_groups LIKE '%\"group\":" + group + ",%' ORDER BY first_name ASC", [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<li>' +
					'<a href="pages/producer.html" data-producer-id="' + record.id + '" class="item-link item-content">' +
						'<div class="item-media">' +
							'<img src="data:image/jpeg;base64,' + record.photo + '" />' +
						'</div>' +
						'<div class="item-inner">' +
							'<div class="item-title-row">' +
								'<div class="item-title">' +
									record.code +
								'</div>' +
							'</div>' +
							'<div class="item-subtitle">' +
								record.first_name + ' ' + record.other_names +
							'</div>' +
						'</div>' +
					'</a>' +
				'</li>';
			}

			$$( '.producers-list-block ul' ).html( html );

			$$( '.producers-list-block ul li a' ).on( 'click', function( event ) {
				producer = $$( this ).attr( 'data-producer-id' );
			} );
		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="group"]', function( event ) {
	loadGroup();
} );

function loadProducers() {
	db.transaction( function( tx ) {
		tx.executeSql( "SELECT id, code, first_name, other_names, photo FROM producers ORDER BY first_name ASC", [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<li>' +
					'<a href="pages/producer.html" data-producer-id="' + record.id + '" class="item-link item-content">' +
						'<div class="item-media">' +
							'<img src="data:image/jpeg;base64,' + record.photo + '" />' +
						'</div>' +
						'<div class="item-inner">' +
							'<div class="item-title-row">' +
								'<div class="item-title">' +
									record.code +
								'</div>' +
							'</div>' +
							'<div class="item-subtitle">' +
								record.first_name + ' ' + record.other_names +
							'</div>' +
						'</div>' +
					'</a>' +
				'</li>';
			}

			$$( '.producers-list-block ul' ).html( html );

			$$( '.producers-list-block ul li a' ).on( 'click', function( event ) {
				producer = $$( this ).attr( 'data-producer-id' );
			} );
		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="producers"]', function( event ) {
	loadProducers();
} );

$$( document ).on( 'page:init', '.page[data-page="add-producer"]', function( event ) {

    db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id, name FROM banks ', [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<option value="' + record.id + '">' +
					record.name +
				'</option>';
			}

			$$( '#producer-bank' ).html( html ).trigger( 'change' );
		} );
	} );

	$$( '#producer-bank' ).off( 'change' ).on( 'change', function( event ) {
    		var bankId = $$( this ).val();

    		if ( bankId != '' ) {
    			db.transaction( function( tx ) {
    				tx.executeSql( 'SELECT id, name FROM bank_branches WHERE bank_id = ?', [ bankId ], function( tx, result ) {
    					if ( result.rows.length > 0 ) {
    						var html = '';

    						for ( i = 0; i < result.rows.length; i++ ) {
    							var record = result.rows.item( i );

    							html += '<option value="' + record.id + '">' +
    								record.name +
    							'</option>';
    						}

    						$$( '#bank-branch' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
    					}
    				} );
    			} );
    		}
    } );

	$$( '#bank_method' ).off( 'change' ).on( 'change', function( event ) {
    		var bankId = $$( this ).val();

    		if ( bankId == 1  ) {
                $$('.bank').show();
                $$('.bank-branch').show();
                $$('.account-names').show();
                $$('.account-no').show();
    		    $$('.sacco').hide();
    		} else {
    		    $$('.sacco').show();
    		    $$('.bank').hide();
    		    $$('.bank-branch').hide();
    		    $$('.account-names').hide();
    		    $$('.account-no').hide();
    		}
    } );

    db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, name FROM producer_groups ', [], function( tx, result ) {
                var html = '';

                for ( i = 0; i < result.rows.length; i++ ) {
                    var record = result.rows.item( i );

                    html += '<option value="' + record.id + '">' +
                        record.name +
                    '</option>';
                }

                $$( '#producer-group' ).html( html ).trigger( 'change' );
            } );
        } );
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<option value="' + record.id + '">' +
					record.name +
				'</option>';
			}

			$$( '#producer-district' ).html( html ).trigger( 'change' );
		} );
	} );

	$$( '#group-name' ).on( 'blur', function( event ) {
		if ( $$( this ).val().trim().length == 0 )
			$$( this ).focus();
	} ).focus();

	$$( '#producer-district' ).off( 'change' ).on( 'change', function( event ) {
		var districtId = $$( this ).val();

		db.transaction( function( tx ) {
			tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ?', [ districtId ], function( tx, result ) {
				if ( result.rows.length > 0 ) {
					var html = '';

					for ( i = 0; i < result.rows.length; i++ ) {
						var record = result.rows.item( i );

						html += '<option value="' + record.id + '">' +
							record.name +
						'</option>';
					}

					$$( '#producer-sub-county' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
				}
			} );
		} );
	} );

	$$( '#producer-sub-county' ).off( 'change' ).on( 'change', function( event ) {
		var subCountyId = $$( this ).val();

		if ( subCountyId != '' ) {
			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ?', [ subCountyId ], function( tx, result ) {
					if ( result.rows.length > 0 ) {
						var html = '';

						for ( i = 0; i < result.rows.length; i++ ) {
							var record = result.rows.item( i );

							html += '<option value="' + record.id + '">' +
								record.name +
							'</option>';
						}

						$$( '#producer-village' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
					}
				} );
			} );
		}
	} );
	
  $$( '#take-picture' ).off( 'click' ).on( 'click tap', function ( event ) {
		event.preventDefault();
		
		var camera_options = {
			quality: 100,
			targetWidth: 300,
			targetHeight: 300,
			allowEdit: true,
			correctOrientation: true,
            destinationType: Camera.DestinationType.DATA_URL,
			sourceType: Camera.PictureSourceType.CAMERA,
			encodingType: Camera.EncodingType.JPEG,
			mediaType: Camera.MediaType.PICTURE,
		};
		
        navigator.camera.getPicture( 
			function ( imageData ) {
			   $$( '#myImage' ).attr( 'src', "data:image/jpeg;base64," + imageData );
			   $$( '#producer_photo' ).val( imageData );
			   
			},
			null, 
			camera_options 
		);
    });
	
	$$( '#save-producer' ).off( 'click' ).on( 'click', function( event ) {
		
		var first_name = $$( '#first_name' ),
		    other_names = $$( '#other_names' ),
			gender = $$( '#gender' ),
			d_0_b = $$( '#d_0_b' ),
			producer_phone_number = $$( '#producer_phone_number' ),
			producer_photo = $$( '#producer_photo' ),
			bank_method = $$( '#bank_method' ),
			producer_bank = $$( '#producer-bank' ),
			bank_branch = $$( '#bank-branch' ),
			bank_account_name = $$( '#producer_bank_name' ),
			bank_account_number = $$( '#producer_bank_account' ),
			producer_sacco = $$( '#producer_sacco' ),
			producer_group = $$( '#producer-group' ),
			position_held = $$( '#position_held' ),
			producer_address = $$( '#producer_address' ),
			district = $$( '#producer-district' ),
			subCounty = $$( '#producer-sub-county' ),
			village = $$( '#producer-village' ),
			producer_groups = '[{"group":'+producer_group.val()+',"position":"'+position_held.val()+'"}]',
            bank_details = '[{"bank":'+producer_bank.val()+',"branch":'+bank_branch.val()+',"account":{"name":"'+ bank_account_name.val() +'","number":"'+ bank_account_number.val() +'"}}]';

		if ( first_name.val().trim().length == 0 )
			first_name.focus();

		else if ( other_names.val().trim().length == 0 )
			other_names.focus();

		else if ( gender.val().trim().length == 0 )
			gender.focus();

		else if ( d_0_b.val().trim().length == 0 )
			d_0_b.focus();

		else if ( producer_phone_number.val().trim().length == 0 )
			producer_phone_number.focus();

		else if ( producer_photo.val().trim().length == 0 )
			producer_photo.focus();

		else if ( bank_method.val().trim().length == 0 ){
			bank_method.focus();
			 if ( bank_method.val() == 1  ){

			        if ( producer_bank.val().trim().length() == 0 .val() == 1 )
            			    producer_bank.focus();

                    if ( bank_branch.val().trim().length() == 0 )
            			    producer_bank.focus();

                    if ( bank_account_name.val().trim().length() == 0 )

            		if ( bank_account_number.val().trim().length() == 0 )
            			    bank_account_number.focus();
              }
              else {

                    if ( producer_sacco.val().trim().length() == 0 .val() == 1 )
                            producer_sacco.focus();

              }
        }
		else if ( producer_group.val().trim().length == 0 )
			producer_group.focus();

        else if ( position_held.val().trim().length == 0 )
			position_held.focus();

		else if ( producer_address.val().trim().length == 0 )
			producer_address.focus();

		else if ( village.val() == '' )
			village.focus();

		else {

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT id FROM producers WHERE first_name = ? AND other_names = ?', [ first_name.val(), other_names.val() ], function( tx, result ) {
					if ( result.rows.length == 0 ) {
						tx.executeSql( 'INSERT INTO producers ( village_id, first_name , other_names , gender , date_of_birth , phone_number , photo , producer_groups , banking_method , sacco_name , bank_details ) VALUES ( ?, ? , ? ,? , ?, ? , ? ,? , ?, ? , ? )', [ parseInt( village.val() ), first_name.val(), other_names.val(), gender.val(), d_0_b.val() , producer_phone_number.val() , producer_photo.val() , producer_groups, bank_method.val(), producer_sacco.val(), bank_details ],
						function( tx, result ) {
							$$( '.back' ).click();

							loadProducers();
						} );
					}
					else {
						app.alert( 'Producer is registered in the system!', 'Error' );

						first_name.focus();
					}
				} );
			} );
		}
	} );
} );

function loadProducer() {

    db.transaction( function( tx ) {
		tx.executeSql( 'SELECT * FROM producers WHERE id = ? LIMIT 1', [ producer ], function( tx, result ) {
			var producer = result.rows.item( 0 );
		    $$( '.producer-names' ).text( result.rows.item( 0 ).first_name + ' ' + result.rows.item( 0 ).other_names  );
		    var html = '';
			if ( result.rows.length == 1 ) {

                var record = result.rows.item( 0 ),
                    producer_groups = JSON.parse ( record.producer_groups ) ,
                    bank_details = JSON.parse ( record.bank_details ) ;
                //    account_details = JSON.parse ( record.bank_details.account ) ;

                    function getGender( id ) {
                        if(id ==1 )
                            return 'Male';
                        else
                            return 'Female';
                    }

                    getGroup ( producer_groups[0].group );

                    function getGroup( id ) {

                         db.transaction( function( tx ) {
                         		tx.executeSql( 'SELECT name FROM producer_groups WHERE id = ? LIMIT 1', [ id ], function( tx, result ) {
                         			if ( result.rows.length == 1 ) {
                         				$$( '.group-name' ).text( result.rows.item( 0 ).name );
                         			}
                         		} );
                          } );

                    }

                    getVillage ( record.village_id );

                    function getVillage( id ) {

                         db.transaction( function( tx ) {
                         		tx.executeSql( 'SELECT name FROM villages WHERE id = ? LIMIT 1', [ id ], function( tx, result ) {
                         			if ( result.rows.length == 1 ) {
                         				$$( '.village' ).text( result.rows.item( 0 ).name );
                         			}
                         		} );
                          } );

                    }

                    getBankingMethod( record.banking_method );

                    function getBankingMethod( id ) {

                        if(id == 1 ) {

                        $$( '.banking-method' ).text( 'Method : Bank' );
                     //   $$( '.account_number' ).text( 'Account Number : ' + account_details.number );
                     //   $$( '.account_name' ).text( 'Account Name : ' + account_details.name );
                        var bank_id = bank_details[0].bank ,
                            branch_id =   bank_details[0].branch ;

                        db.transaction( function( tx ) {
                            tx.executeSql( 'SELECT name FROM banks WHERE id = ? LIMIT 1', [ bank_id ], function( tx, result ) {
                                if ( result.rows.length == 1 ) {
                                    $$( '.bank-name' ).text('Name :  ' + result.rows.item( 0 ).name );
                                }
                            } );
                        } );

                        db.transaction( function( tx ) {
                            tx.executeSql( 'SELECT name FROM bank_branches WHERE id = ? LIMIT 1', [ branch_id ], function( tx, result ) {
                                if ( result.rows.length == 1 ) {
                                    $$( '.branch-name' ).text('Branch :  ' + result.rows.item( 0 ).name );
                                }
                            } );
                        } );

                        }
                        else{
                             $$( '.banking-method' ).text( 'Method : Sacco' );
                             $$( '.bank-name' ).text('Name :  ' + record.sacco_name);
                         }
                    }


                html ='<div class="content-block">' +
                            '<img src="data:image/jpeg;base64,' + record.photo + '" class="img-fluid" />' +
                      '</div>' +
                      '<div class="content-block-title"> Name </div>' +
                      '<div class="content-block">' +
                            '<p>' + record.first_name + ' ' + record.other_names + '</p>' +
                      '</div>'+
                      '<div class="content-block-title"> Gender </div>' +
                      '<div class="content-block">' +
                            '<p>' + getGender( record.gender )  + '</p>' +
                      '</div>'+
                      '<div class="content-block-title">' +
                            'Date of birth' +
                      '</div>' +
                      '<div class="content-block">' +
                            '<p>' + record.date_of_birth + '</p>' +
                      '</div>' +
                      '<div class="content-block-title">' +
                            'Phone number' +
                      '</div>' +
                      '<div class="content-block">' +
                            '<p>' + record.phone_number + '</p>' +
                      '</div>'+
                      '<div class="content-block-title">' +
                            'Village' +
                      '</div>' +
                      '<div class="content-block">' +
                            '<p class="village"></p>' +
                      '</div>'+
                      '<div class="content-block-title">' +
                            'Producer Group' +
                      '</div>' +
                      '<div class="content-block">' +
                            '<p class="group-name"></p>' +
                      '</div>'+
                      '<div class="content-block-title">' +
                            'Position Held' +
                      '</div>' +
                      '<div class="content-block">' +
                            '<p>' + producer_groups[0].position + '</p>' +
                      '</div>'+
                      '<div class="content-block-title">' +
                            'Banking Details' +
                      '</div>'+
                      '<div class="content-block">' +
                             '<p class="banking-method"></p>' +
                             '<p class="bank-name"></p>' +
                             '<p class="branch-name"></p>' +
                             '<p class="account-name"></p>' +
                             '<p class="account-number"></p>' +
                      '</div>';

				  $$( '.page[data-page="producer"] .page-content' ).html( html );
			}
		} );
    } );

}

$$( document ).on( 'page:init', '.page[data-page="producer"] ', function( event ) {
	loadProducer();
} );

function loadAreas() {

	db.transaction( function( tx ) {
		tx.executeSql( "SELECT id, remote_id , producer_id , tgb_area_size, gps , land_Size , village_id FROM areas where id  = ? ORDER BY id DESC", [ 2 ], function( tx, result ) {
			var html = '';
                function getVillage( id ) {

                     db.transaction( function( tx ) {
                            tx.executeSql( 'SELECT name FROM villages WHERE id = ? LIMIT 1', [ id ], function( tx, result ) {
                                if ( result.rows.length == 1 ) {
                                    $$('.village').text( result.rows.item( 0 ).name );
                                }
                            } );
                      } );

                }
                function getProducer( id ) {

                                     db.transaction( function( tx ) {
                                            tx.executeSql( 'SELECT * FROM producers WHERE id = ? ', [ id ], function( tx, result ) {
                                                for ( i = 0; i < result.rows.length; i++ ) {
                                                    $$('.producer-name').text( result.rows.item( 0 ).first_name + ' ' + result.rows.item( 0 ).other_names );
                                                }
                                            } );
                                      } );

                                }

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

                getVillage( result.rows.item( i ).village_id )

                getProducer( result.rows.item( i ).producer_id );


				html += '<li class="item-content">' +
						'<div class="item-inner">' +
							'<div class="item-title-row">' +
								'<div class="item-title">' +
									record.gps +
								'</div>' +
							'</div>' +
							'<div class="item-subtitle">' +
								record.tgb_area_size + ' ha | ' + record.land_Size  + ' ha ' +
							'</div>' +
						'</div>' +
				'</li>';
			}

			$$( '.areas-list-block ul' ).html( html );


		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="areas"]', function( event ) {
	loadAreas();
} );

$$( document ).on( 'page:init', '.page[data-page="add-area"]', function( event ) {

    db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, first_name , other_names FROM producers ORDER BY first_name ASC ', [ ], function( tx, result ) {
                var html = '';

                for ( i = 0; i < result.rows.length; i++ ) {
                    var record = result.rows.item( i );

                    html += '<option value="' + record.id + '">' +
                        record.first_name + ' ' + record.other_names +
                    '</option>';
                }

                $$( '#producer-name' ).html( html ).trigger( 'change' );
            } );
        } );
	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

				html += '<option value="' + record.id + '">' +
					record.name +
				'</option>';
			}

			$$( '#area-district' ).html( html ).trigger( 'change' );
		} );
	} );

	$$( '#area-district' ).off( 'change' ).on( 'change', function( event ) {
		var districtId = $$( this ).val();

		db.transaction( function( tx ) {
			tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ?', [ districtId ], function( tx, result ) {
				if ( result.rows.length > 0 ) {
					var html = '';

					for ( i = 0; i < result.rows.length; i++ ) {
						var record = result.rows.item( i );

						html += '<option value="' + record.id + '">' +
							record.name +
						'</option>';
					}

					$$( '#area-sub-county' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
				}
			} );
		} );
	} );

	$$( '#area-sub-county' ).off( 'change' ).on( 'change', function( event ) {
		var subCountyId = $$( this ).val();

		if ( subCountyId != '' ) {
			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ?', [ subCountyId ], function( tx, result ) {
					if ( result.rows.length > 0 ) {
						var html = '';

						for ( i = 0; i < result.rows.length; i++ ) {
							var record = result.rows.item( i );

							html += '<option value="' + record.id + '">' +
								record.name +
							'</option>';
						}

						$$( '#area-village' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
					}
				} );
			} );
		}
	} );
    $$('#get-gps').off( 'click' ).on( 'click' , function ( event ){

        navigator.geolocation.getCurrentPosition(onSuccess, onError);

    } );

	$$( '#save-area' ).off( 'click' ).on( 'click', function( event ) {
		var producer_name = $$( '#producer-name' ),
		    tgb_area_size = $$( '#tgb-area-size' ),
			area_gps = $$( '#get_area_gps' ),
			total_land_size = $$( '#total-land-size' ),
			district = $$( '#area-district' ),
			subCounty = $$( '#area-sub-county' ),
			village = $$( '#area-village' );

		if ( producer_name.val().trim().length == 0 )
			producer_name.focus();

		else if ( tgb_area_size.val() == '' )
			tgb_area_size.focus();

		else if ( area_gps.val() == '' )
			area_gps.focus();

        else if ( total_land_size.val() == '' )
            total_land_size.focus();

        else if ( village.val() == '' )
            village.focus();

		else {
			db.transaction( function( tx ) {
				/*tx.executeSql( 'SELECT id FROM producers WHERE id = ?', [  producer_name.val()   ], function( tx, result ) {
					if ( result.rows.length == 0 ) {
				*/		tx.executeSql( 'INSERT INTO areas ( producer_id , tgb_area_size, gps , land_Size , village_id  ) VALUES ( ?, ? , ? , ? ,? )', [ producer_name.val()  , tgb_area_size.val() , area_gps.val() , total_land_size.val() ,  village.val()  ], function( tx, result ) {
							$$( '.back' ).click();

							loadAreas();
						} );
					/*}

					else {
						app.alert( 'Producer doesn\'t  exist. Try again!', 'Error' );

						producer_name.focus();
					}
				} );*/
			} );
		}
	} );
} );

 var onSuccess = function(position) {
                  $$('#get_area_gps').val(position.coords.latitude + ' ' +position.coords.longitude )
              };

function onError(error) {
  alert('code: '    + error.code    + '\n' +
        'message: ' + error.message + '\n');
}
function loadPlantings() {

	db.transaction( function( tx ) {
		tx.executeSql( "SELECT id, remote_id , producer_id , tgb_area_size, gps , land_Size , village_id FROM areas ORDER BY id DESC", [], function( tx, result ) {
			var html = '';

			for ( i = 0; i < result.rows.length; i++ ) {
				var record = result.rows.item( i );

                getVillage( record.village_id )

                function getVillage( id ) {

                     db.transaction( function( tx ) {
                            tx.executeSql( 'SELECT name FROM villages WHERE id = ? LIMIT 1', [ id ], function( tx, result ) {
                                if ( result.rows.length == 1 ) {
                                    $$('.village').text( result.rows.item( 0 ).name );
                                }
                            } );
                      } );

                }

                getProducer( record.producer_id );

                function getProducer( id ) {

                     db.transaction( function( tx ) {
                            tx.executeSql( 'SELECT * FROM producers WHERE id = ? ', [ id ], function( tx, result ) {
                                for ( i = 0; i < result.rows.length; i++ ) {
                                    $$('.producer-name').text( result.rows.item( 0 ).first_name + ' ' + result.rows.item( 0 ).other_names );
                                }
                            } );
                      } );

                }

				html += '<li class="item-content">' +
						'<div class="item-inner">' +
							'<div class="item-title-row">' +
								'<div class="item-title">' +
									'<span class="producer-name"></span>' +
								'</div>' +
							'</div>' +
							'<div class="item-subtitle">' +
								record.tgb_area_size + ' ' + record.land_Size + ' ' + record.gps + ' ' + '<span class="village"></span>' +
							'</div>' +
						'</div>' +
				'</li>';
			}

			$$( '.areas-list-block ul' ).html( html );


		} );
	} );
}

$$( document ).on( 'page:init', '.page[data-page="plantings"]', function( event ) {
	loadPlantings();
} );

$$( document ).on( 'page:init', '.page[data-page="add-planting"]', function( event ) {

    db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, first_name , other_names FROM producers ORDER BY first_name ASC ', [ ], function( tx, result ) {
                var html = '';

                for ( i = 0; i < result.rows.length; i++ ) {
                    var record = result.rows.item( i );

                    html += '<option value="' + record.id + '">' +
                        record.first_name + ' ' + record.other_names +
                    '</option>';
                }

                $$( '#producer-name' ).html( html ).trigger( 'change' );
            } );
        } );

	$$( '#producer-name' ).off( 'change' ).on( 'change', function( event ) {
		var producerId = $$( this ).val();

		db.transaction( function( tx ) {
			tx.executeSql( 'SELECT *  FROM areas WHERE producer_id = ?', [ producerId ], function( tx, result ) {
				if ( result.rows.length > 0 ) {
					var html = '';

					for ( i = 0; i < result.rows.length; i++ ) {
						var record = result.rows.item( i );

						html += '<option value="' + record.id + '">' +
							record.gps +
						'</option>';
					}

					$$( '#planting-area' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
				}
			} );
		} );
	} );

    db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, name FROM tech_specifications ORDER BY name ASC ', [ ], function( tx, result ) {
                var html = '';

                for ( i = 0; i < result.rows.length; i++ ) {
                    var record = result.rows.item( i );

                    html += '<option value="' + record.id + '">' +
                        record.name  +
                    '</option>';
                }

                $$( '#tech-spec-versions' ).html( html ).trigger( 'change' );
            } );
    } );

    $$( '#tech-spec-versions' ).off( 'change' ).on( 'change', function( event ) {
		var versionId = $$( this ).val();

		db.transaction( function( tx ) {
				tx.executeSql( 'SELECT id, name FROM tech_specification_descriptions WHERE tech_specification_id = ?', [ versionId ], function( tx, result ) {
				if ( result.rows.length > 0 ) {
					var html = '';

					for ( i = 0; i < result.rows.length; i++ ) {
						var record = result.rows.item( i );

						html += '<option value="' + record.id + '">' +
							record.name +
						'</option>';
					}

					$$( '#tech-spec-description' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
				}
			} );
		} );
	} );

	$$( '#save-planting' ).off( 'click' ).on( 'click', function( event ) {
		var producer_name = $$( '#producer-name' ),
		    tgb_area_size = $$( '#tgb-area-size' ),
			area_gps = $$( '#get_area_gps' ),
			total_land_size = $$( '#total-land-size' ),
			district = $$( '#area-district' ),
			subCounty = $$( '#area-sub-county' ),
			village = $$( '#area-village' );

		if ( producer_name.val().trim().length == 0 )
			producer_name.focus();

		else if ( tgb_area_size.val() == '' )
			tgb_area_size.focus();

		else if ( area_gps.val() == '' )
			area_gps.focus();

        else if ( total_land_size.val() == '' )
            total_land_size.focus();

        else if ( village.val() == '' )
            village.focus();

		else {
			db.transaction( function( tx ) {
				/*tx.executeSql( 'SELECT id FROM producers WHERE id = ?', [  producer_name.val()   ], function( tx, result ) {
					if ( result.rows.length == 0 ) {
				*/		tx.executeSql( 'INSERT INTO areas ( producer_id , tgb_area_size, gps , land_Size , village_id  ) VALUES ( ?, ? , ? , ? ,? )', [ producer_name.val()  , tgb_area_size.val() , area_gps.val() , total_land_size.val() ,  village.val()  ], function( tx, result ) {
							$$( '.back' ).click();

							loadAreas();
						} );
					/*}

					else {
						app.alert( 'Producer doesn\'t  exist. Try again!', 'Error' );

						producer_name.focus();
					}
				} );*/
			} );
		}
	} );
} );

$$( document ).on( 'page:init', '.page[data-page="choose-monitoring"]', function( event ) {

    db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, first_name , other_names FROM producers ORDER BY first_name ASC ', [ ], function( tx, result ) {
                var html = '';

                for ( i = 0; i < result.rows.length; i++ ) {
                    var record = result.rows.item( i );

                    html += '<option value="' + record.id + '">' +
                        record.first_name + ' ' + record.other_names +
                    '</option>';
                }

                $$( '#producer-name' ).html( html ).trigger( 'change' );
            } );
        } );

    $$( '#producer-name' ).off( 'change' ).on( 'change', function( event ) {
    		var producerId = $$( this ).val();

    		db.transaction( function( tx ) {
    			tx.executeSql( 'SELECT  * FROM areas WHERE producer_id = ?', [ producerId ], function( tx, result ) {
    				if ( result.rows.length > 0 ) {
    					var html = '';

    					for ( i = 0; i < result.rows.length; i++ ) {
    						var record = result.rows.item( i );

    						html += '<option value="' + record.id + '">' +
    							record.gps +
    						'</option>';
    					}

    					$$( '#area' ).html( html ).removeAttr( 'disabled' ).trigger( 'change' );
    				}
    			} );
    		} );
    	} );

	$$( '#save-area' ).off( 'click' ).on( 'click', function( event ) {
		var producer_name = $$( '#producer-name' ),
		    area_size = $$( '#area-size' );


		if ( first_name.val().trim().length == 0 )
			first_name.focus();

		else if ( village.val() == '' )
			village.focus();

		else {

			db.transaction( function( tx ) {
				tx.executeSql( 'SELECT id FROM producers WHERE first_name = ? AND other_names = ?', [ parseInt( first_name.val() ), other_names.val()  ], function( tx, result ) {
					if ( result.rows.length == 0 ) {
						tx.executeSql( 'INSERT INTO producers ( village_id, first_name , other_names , gender , date_of_birth , phone_number , photo , producer_groups , banking_method , sacco_name , bank_details ) VALUES ( ?, ? , ? ,? , ?, ? , ? ,? , ?, ? , ? )', [ parseInt( village.val() ), first_name.val() , other_names.val() ,gender.val() , d_0_b.val() , producer_phone_number.val() , producer_photo.val() , producer_groups , bank_method.val() , producer_sacco.val() ,bank_details ], function( tx, result ) {
							$$( '.back' ).click();

							loadProducers();
						} );
					}
					else {
						app.alert( 'Producer group already exists. Try again!', 'Error' );

						first_name.focus();
					}
				} );
			} );
		}
	} );
} );

